let state = "Saludando"; // or "Despidiendo"
const msgRef = document.getElementById("msg");
const btnRef = document.getElementById("btn");

function renderMsg() {
  if (state === "Saludando") {
    msgRef.innerHTML = "Te saludo gustosamente";
    btnRef.innerHTML = "Despedirse";
    state = "Despidiendo";
  } else {
    msgRef.innerHTML = "Me despido cordialmente";
    btnRef.innerHTML = "Saludar";
    state = "Saludando";
  }
}

renderMsg();
btnRef.addEventListener("click", renderMsg);

// ----------------------------------------------------
let numOfCalls = 0;
const counterRef = document.getElementById("counter");
const statusRef = document.getElementById("status");
document.getElementById("fetch-btn").addEventListener("click", async () => {
  counterRef.innerHTML = ++numOfCalls + "";
  statusRef.innerHTML = "loading...";

  // esto añade un delay ficticio para que sea más vistosa la llamada
  await sleep(1500); // segundo y medio

  const resp = await fetch("https://seattle.juliocastrodev.duckdns.org/status");
  const respJson = await resp.json();
  statusRef.innerHTML = `El estado del servidor es: ${respJson.status}`;
});

// ignorar esta función, es simplemente para meter delay
function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), ms);
  });
}
