package app.controller;

import app.model.Busqueda;
import app.service.MetricasMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ViewController {

  @Autowired
  MetricasMail metricasMail;

  @GetMapping("/home")
  public String home(Model model) throws Exception {
    model.addAttribute(
      "busquedas",
      List.of(
        new Busqueda(1, "test 1", 100L),
        new Busqueda(2, "prueba 2", 300L  )
      )
    );
    model.addAttribute("mensaje", "hola desde spring boot");
    return "home";
  }

  @GetMapping("/metricas")
  public String metricas(Model model) throws Exception {
    model.addAttribute("consultas", metricasMail.getNumConsultas());
    model.addAttribute("pcr", metricasMail.numPCR());
    model.addAttribute("covid", metricasMail.numPacientesCovid());
    model.addAttribute("enfermedad", metricasMail.getMaxSickness().getFirst());
    model.addAttribute("nEnfermedad", metricasMail.getMaxSickness().getSecond());
    model.addAttribute("medicamento", metricasMail.getMaxMeds().getFirst());
    model.addAttribute("nMed", metricasMail.getMaxMeds().getSecond());
    model.addAttribute("prueba", metricasMail.getMaxLabTests().getFirst());
    model.addAttribute("nPrueba", metricasMail.getMaxLabTests().getSecond());
    return "metricas";
  }
}
