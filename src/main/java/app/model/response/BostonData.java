package app.model.response;

import java.util.List;

import app.model.BostonConsulta;
import app.model.Metadata;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BostonData {

  private List<BostonConsulta> consulta;
  private Metadata infoPagina;

}
