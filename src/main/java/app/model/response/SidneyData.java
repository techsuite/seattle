package app.model.response;

import java.util.List;

import app.model.SidneyConsulta;
import app.model.SidneyEmpleado;
import app.model.SidneyPaciente;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class SidneyData {

    private String key = "W6YRf#*ZLsZ1";
    private List<SidneyEmpleado> employees;
    private List<SidneyPaciente> patients;
    private List<SidneyConsulta> consultas;

}