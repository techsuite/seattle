package app.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BostonConsulta {

  private int idConsulta;
  private String fechaRegistroPaciente;
  private String fechaConsulta;
  private String enfermedadesDiagnosticadas;
  private String medicamentosRecetados;
  private String pruebasDeLaboratorioARealizar;
  private String observaciones;
  private String dniDoctor;
  private String nombreDoctor;
  private String apellidosDoctor;
  private String fechaNacimientoDoctor;
  private String correoElectronicoDoctor;
  private String generoDoctor;
  private String passwordDoctor;
  private String dniPaciente;
  private String nombrePaciente;
  private String apellidosPaciente;
  private String fechaNacimientoPaciente;
  private String correoElectronicoPaciente;
  private String generoPaciente;
}
