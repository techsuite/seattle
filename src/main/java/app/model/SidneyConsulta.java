package app.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class SidneyConsulta {

  private int idConsulta;
  private String dniDoctor;
  private String dniPaciente;
  private String fechaRegistroPaciente;
  private String fechaConsulta;
  private String[] enfermedadesDiagnosticadas;
  private String[] medicamentosRecetados;
  private String[] pruebasLabRealizar;
  private String[] observaciones;

}