package app.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class SidneyPaciente {

    private String dni;
    private String nombre;
    private String apellidos;
    private String fechaNacimiento;
    private String correoElectronico;
    private String genero;
    private String password = "";

}