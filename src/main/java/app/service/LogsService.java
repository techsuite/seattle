package app.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogsService {

  private static Logger LOGGER = LoggerFactory.getLogger(LogsService.class);

  public void basic(String str) {
    LOGGER.info(str);
  }

  public void extractionError(int errorCode) {
    LOGGER.error("======= ERROR EN LA EXTRACCION. CODIGO ERROR" + errorCode + " =======");
  }

  public void pingError(int errorCode) {
    LOGGER.error("Connection failed, error code: " + errorCode);
  }

  public void firstExtractionStart() {
    LOGGER.info("======= INICIO EXTRACCION PAGINA 1 =======");
  }

  public void firstExtractionEnd() {
    LOGGER.info("======= EXTRACCION PAGINA 1 COMPLETADA =======");
  }

  public void nExtractionStart(int n, int m) {
    LOGGER.info("======= INICIO EXTRACCION PAGINA " + n + " DE " + m + " ======="
    );
  }

  public void nExtractionEnd(int n, int m) {
    LOGGER.info("======= EXTRACCION PAGINA " + n + " DE " + m + " COMPLETADA ======="
    );
  }

  public void nTransformStart(int n, int m) {
    LOGGER.info("======= INICIO TRANSFORMACION PAGINA " + n + " DE " + m + " ======="
    );
  }

  public void nTransformEnd(int n, int m) {
    LOGGER.info("======= TRANSFORMACION PAGINA " + n + " DE " + m + " COMPLETADA ======="
    );
  }

  public void nLoadStart(int n, int m) {
    LOGGER.info("======= INICIO ENVIO SIDNEY PAGINA " + n + " DE " + m + " ======="
    );
  }

  public void loadError(int errorCode) {
    LOGGER.error("======= ERROR EN EL ENVIO. CODIGO ERROR" + errorCode + " ======="
    );
  }

  public void nLoadEnd(int n, int m) {
    LOGGER.info("======= ENVIO PAGINA " + n + " DE " + m + " COMPLETADA ======="
    );
  }

}
