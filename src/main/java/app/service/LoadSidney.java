package app.service;

import okhttp3.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoadSidney {

    public static final MediaType JSON = MediaType.parse(
            "application/json; charset=utf-8"
    );

    @Autowired
    private OkHttpClient client;

    @Autowired
    private LogsService logsService;

    public void load(String post, int n, int m) throws Exception {
        System.out.println(post);

        logsService.nLoadStart(n, m);

        RequestBody body = RequestBody.create(JSON, String.valueOf(post));
        Request request = new Request.Builder()
                .url("https://sidney.juliocastrodev.duckdns.org/getDataSeattle")
                .post(body)
                .build();

        Call call = client.newCall(request);
        Response response = call.execute();

        if (response.code()!=200)
            logsService.loadError(response.code());
        else
            logsService.nLoadEnd(n, m);
    }
}
