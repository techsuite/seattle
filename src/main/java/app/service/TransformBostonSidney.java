package app.service;

import java.util.List;
import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.model.BostonConsulta;
import app.model.SidneyConsulta;
import app.model.SidneyEmpleado;
import app.model.SidneyPaciente;
import app.model.response.BostonData;
import app.model.response.SidneyData;
import org.apache.commons.lang3.StringUtils;

@Service
public class TransformBostonSidney {

    @Autowired
    MetricasMail metricasMail;

    public SidneyData transform(BostonData pagina) {

        List<SidneyConsulta> sidneyConsultaList = new ArrayList<SidneyConsulta>();
        List<SidneyEmpleado> sidneyEmpleadoList = new ArrayList<SidneyEmpleado>();
        List<SidneyPaciente> sidneyPacienteList = new ArrayList<SidneyPaciente>();

        transformConsultas(sidneyConsultaList, pagina);
        transformPacientes(sidneyPacienteList, pagina);
        transformEmpleados(sidneyEmpleadoList, pagina);

        SidneyData data = new SidneyData();
        data.setConsultas(sidneyConsultaList);
        data.setEmployees(sidneyEmpleadoList);
        data.setPatients(sidneyPacienteList);
        data.setKey("W6YRf#*ZLsZ1");

        metricasMail.update(data);

        return data;
        
    }

    private void transformEmpleados(List<SidneyEmpleado> list, BostonData pagina) {
        
        SidneyEmpleado object = new SidneyEmpleado();

        for(BostonConsulta data : pagina.getConsulta()){
            
            object.setDni(data.getDniDoctor().equals(" ") ? "" : data.getDniDoctor());
            object.setNombre(data.getNombreDoctor().equals(" ") ? "" : data.getNombreDoctor());
            object.setApellidos(data.getApellidosDoctor().equals(" ") ? "" : data.getApellidosDoctor());
            object.setFechaNacimiento(data.getFechaNacimientoDoctor().equals(" ") ? "" : data.getFechaNacimientoDoctor());
            object.setGenero(data.getGeneroDoctor().equals(" ") ? "" : StringUtils.stripAccents(data.getGeneroDoctor()));
            object.setCorreoElectronico(data.getCorreoElectronicoDoctor().equals(" ") ? "" : data.getCorreoElectronicoDoctor());
            object.setPassword(data.getPasswordDoctor().equals(" ") ? "" : data.getPasswordDoctor());
            list.add(object);

        }

    }

    private void transformPacientes(List<SidneyPaciente> list, BostonData pagina) {
        
        SidneyPaciente object = new SidneyPaciente();

        for(BostonConsulta data : pagina.getConsulta()){

            object.setDni(data.getDniPaciente().equals(" ") ? "" : data.getDniPaciente());
            object.setNombre(data.getNombrePaciente().equals(" ") ? "" : data.getNombrePaciente());
            object.setApellidos(data.getApellidosPaciente().equals(" ") ? "" : data.getApellidosPaciente());
            object.setFechaNacimiento(data.getFechaNacimientoPaciente().equals(" ") ? "" : data.getFechaNacimientoPaciente());
            object.setGenero(data.getGeneroPaciente().equals(" ") ? "" : StringUtils.stripAccents(data.getGeneroPaciente()));
            object.setCorreoElectronico(data.getCorreoElectronicoPaciente().equals(" ") ? "" : data.getCorreoElectronicoPaciente());
            list.add(object);

        }

    }

    private void transformConsultas(List<SidneyConsulta> list, BostonData pagina) {

        SidneyConsulta object = new SidneyConsulta();

        for(BostonConsulta data : pagina.getConsulta()){

            object.setIdConsulta(data.getIdConsulta());
            object.setDniPaciente(data.getDniPaciente().equals(" ") ? "" : data.getDniPaciente());
            object.setDniDoctor(data.getDniDoctor().equals(" ") ? "" : data.getDniDoctor());
            object.setFechaConsulta(data.getFechaConsulta().equals(" ") ? "" : data.getFechaConsulta());
            object.setFechaRegistroPaciente(data.getFechaRegistroPaciente().equals(" ") ? "" : data.getFechaRegistroPaciente());

            object.setObservaciones(!data.getObservaciones().equals(" ") ? data.getObservaciones().split("\\.") : new String[]{} );
            object.setEnfermedadesDiagnosticadas((!data.getEnfermedadesDiagnosticadas().equals(" ")) ? data.getEnfermedadesDiagnosticadas().split("\\.") : new String[]{});
            object.setPruebasLabRealizar(!data.getPruebasDeLaboratorioARealizar().equals(" ") ? StringUtils.stripAccents(data.getPruebasDeLaboratorioARealizar()).split("\\.") : new String[]{});
            object.setMedicamentosRecetados(!data.getMedicamentosRecetados().equals(" ") ? data.getMedicamentosRecetados().split("\\.") : new String[]{});
            
            list.add(object);

        }

    }
    
    
}
