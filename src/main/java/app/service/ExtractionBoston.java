package app.service;

import app.model.response.SidneyData;
import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import app.model.response.BostonData;
import okhttp3.*;


@Service
public class ExtractionBoston {

    @Autowired
    private OkHttpClient client;

    @Autowired
    private LogsService logsService;

    private int errorCode = 200;

    @Autowired
    private ConexionBoston conexion;

    @Autowired
    private LoadSidney loadSidney;

    @Autowired
    private TransformBostonSidney transformBSidney;

    private BostonData getPagina(int pageNumber, int pageSize, int numIntentos) throws Exception {

        if (numIntentos==0) {
            logsService.extractionError(errorCode);
            return null;
        }

        Request request = new Request.Builder()
                .url(
                        "https://boston-be.juliocastrodev.duckdns.org/consultas?pageNumber=" +
                                pageNumber +
                                "&pageSize=" +
                                pageSize
                )
                .build();
        Call call = client.newCall(request);
        Response response = call.execute();
        errorCode = response.code();

        if (errorCode != 200 && numIntentos > 0) {
            return getPagina(pageNumber, pageSize, --numIntentos);
        }
        return new Gson().fromJson(response.body().string(), BostonData.class);
    }

    @Scheduled(cron = "0 0 */1 * * *")
    //   @Scheduled(fixedRate = 180000) //está cada 3 min (para la demo) y hay que cambiarlo a cada hora
    public void extraction() throws Exception {

        //En caso de que el ping a Boston falle dos veces, error al establecer conexion.
        if (!conexion.pingBoston(2)) {
            errorCode = 404;
            logsService.extractionError(errorCode);
            return;
        }

        //Extraemos y procesamos la primera pagina a parte para poder obtener totalPages antes de iterar.
        logsService.firstExtractionStart();
        BostonData pagina = getPagina(1, 1, 2);

        if (pagina == null)
            return;

        logsService.firstExtractionEnd();

        int totalPages = pagina.getInfoPagina().getTotalPages();

        SidneyData sidneyData;
        logsService.nTransformStart(1, totalPages);
        sidneyData = transformBSidney.transform(pagina);
        logsService.nTransformEnd(1, totalPages);

        loadSidney.load(new GsonBuilder().setPrettyPrinting().create().toJson(sidneyData), 1, totalPages);

      /*
      De cara a ahorrar recursos, si bien en nuestro caso tal vez no seria necesario,
      no guardamos los datos en un array sino que simplemente procesamos pagina a pagina
      y directamente se transforma.
      */

        for (int i = 1; i < totalPages; i++) {
            logsService.nExtractionStart(i+1, totalPages);
            pagina = getPagina(i + 1, 1,2);

            if (pagina != null) {
                logsService.nExtractionEnd(i+1, totalPages);
                logsService.nTransformStart(i+1, totalPages);
                sidneyData = transformBSidney.transform(pagina);
                logsService.nTransformEnd(i+1, totalPages);

                loadSidney.load(new GsonBuilder().setPrettyPrinting().create().toJson(sidneyData), i+1, totalPages);
            }
        }
    }
}
