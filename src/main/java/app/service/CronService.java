package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

//TODO AÑADIR EXTRACCION Y TODAS LOS CRONOMETROS FUTUROS AQUI, ACTUALIZAR TESTS DE CRONTEST CADA VEZ QUE SE AÑADA ALGO
@Service
public class CronService {

    @Autowired
    Mail mail;

    @Autowired
    MetricasMail metricasMail;
    //cronometro cada 3 minutos para la demo, cambiar 24 horas para version final
    @Scheduled(cron = "0 0 */1 * * *")
    public void metricSending(){
        try {
            mail.sendMail(
                    "developer.techsuite@outlook.com",
                    "Metricas ultimas 24h",
                    "<div><h1>Techsuite</h1><p>Se han realizado " + metricasMail.getNumConsultas() + " consultas en estas 24h " +
                            "<br>" +
                            "La enfermedad más común estas 24h ha sido "+ metricasMail.getMaxSickness().getFirst()+" con "+metricasMail.getMaxSickness().getSecond()+" incidencias." +
                            "<br>" +
                            "El medicamento más recetado estas 24h ha sido " + metricasMail.getMaxMeds().getFirst()+" con "+metricasMail.getMaxMeds().getSecond()+" incidencias."+
                            "<br>" +
                            "La prueba más realizada estas 24h ha sido "+ metricasMail.getMaxLabTests().getFirst()+" con "+metricasMail.getMaxLabTests().getSecond()+" incidencias." +
                            "<br>" +
                            "Se han realizado " + metricasMail.numPCR() + " pruebas PCR. " +
                            "<br>" +
                            "Se han detectado " + metricasMail.numPacientesCovid() + " positivos por coronavirus estas últimas 24h. " +
                            "</p></div>"
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        metricasMail.restart();

    }
}
