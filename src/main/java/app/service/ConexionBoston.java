package app.service;

import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
class ConexionBoston {

  @Autowired
  private OkHttpClient client;

  @Autowired
  private ConexionSidney sidney;

  @Autowired
  private LogsService logsService;

  private int errorCode = 200;

  public boolean pingBoston(int numIntentos) throws Exception {

    if (numIntentos == 0)
      return false;

    Request request = new Request.Builder()
      .url("https://boston-be.juliocastrodev.duckdns.org/status")
      .build();
    Call call = client.newCall(request);
    Response response = call.execute();
    errorCode = response.code();

    if (errorCode != 200 && numIntentos > 0) {
      logsService.pingError(errorCode);
      return pingBoston(--numIntentos);
    }
    return true;
  }

  
}
