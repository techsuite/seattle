package app.service;

import app.model.response.SidneyData;
import kotlin.Pair;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Getter
@Service
public class MetricasMail {
    private Map<String, Integer> enfermedades = new HashMap<>();
    private Map<String, Integer> medicamentos = new HashMap<>();
    private Map<String, Integer> pruebas = new HashMap<>();
    private Pair<String, Integer> maxSickness = new Pair<>("",0);
    private Pair<String, Integer> maxMeds = new Pair<>("",0);
    private Pair<String, Integer> maxLabTests = new Pair<>("",0);
    private int numConsultas;
    private int maxEnfermedades=0;
    private int maxMedicamentos=0;
    private int maxPruebas=0;

    public void update(SidneyData datos){
        updateEnfermedades(datos);
        updateMedicamentos(datos);
        updatePruebas(datos);
        numConsultas = numConsultas + datos.getConsultas().size();
    }

    private void updateEnfermedades(SidneyData datos) {
        for(int i = 0; i<datos.getConsultas().size(); i++){
            String[] sickness = datos.getConsultas().get(i).getEnfermedadesDiagnosticadas();
            for(int j=0; j<sickness.length;j++){
                if(!enfermedades.containsKey(sickness[j]))
                    enfermedades.put(sickness[j],1);
                else
                    enfermedades.put(sickness[j],enfermedades.get(sickness[j])+1);

                if(enfermedades.containsKey(sickness[j]) && enfermedades.get(sickness[j])>maxEnfermedades){
                    maxEnfermedades = enfermedades.get(sickness[j]);
                    maxSickness = new Pair<>(sickness[j],maxEnfermedades);
                }
            }
        }
    }

    private void updateMedicamentos(SidneyData datos) {
        for(int i = 0; i<datos.getConsultas().size(); i++){
            String[] meds = datos.getConsultas().get(i).getMedicamentosRecetados();
            for(int j=0; j<meds.length;j++){
                if(!medicamentos.containsKey(meds[j]))
                    medicamentos.put(meds[j],1);
                else
                    medicamentos.put(meds[j],medicamentos.get(meds[j])+1);

                if(medicamentos.containsKey(meds[j]) && medicamentos.get(meds[j])>maxMedicamentos){
                    maxMedicamentos = medicamentos.get(meds[j]);
                    maxMeds = new Pair<>(meds[j],maxMedicamentos);
                }
            }
        }
    }

    private void updatePruebas(SidneyData datos) {
        for(int i = 0; i<datos.getConsultas().size(); i++){
            String[] labTests = datos.getConsultas().get(i).getPruebasLabRealizar();
            for(int j=0; j<labTests.length;j++){
                if(!pruebas.containsKey(labTests[j]))
                    pruebas.put(labTests[j],1);
                else
                    pruebas.put(labTests[j],pruebas.get(labTests[j])+1);

                if(pruebas.containsKey(labTests[j]) && pruebas.get(labTests[j])>maxPruebas){
                    maxPruebas= pruebas.get(labTests[j]);
                    maxLabTests = new Pair<>(labTests[j],maxPruebas);
                }
            }
        }
    }

    public int getNumSickness(){
        int res = 0;
        for(String key : enfermedades.keySet()){
            res = res + enfermedades.get(key);
        }
        return res;
    }

    public int getNumMeds(){
        int res = 0;
        for(String key : medicamentos.keySet()){
            res = res + medicamentos.get(key);
        }
        return res;
    }

    public int getNumLabTests(){
        int res = 0;
        for(String key : pruebas.keySet()){
            res = res + pruebas.get(key);
        }
        return res;
    }

    public int getNumConsultas(){
        return numConsultas;
    }

    public int numPCR(){
        int res = 0;
        for(String key : pruebas.keySet()){
            if(key.equals("pcr")){
                res = pruebas.get(key);
            }
        }
        return res;
    }

    public int numPacientesCovid(){
        int res = 0;
        for(String key : enfermedades.keySet()){
            if(key.equals("covid-19")){
                res = enfermedades.get(key);
            }
        }
        return res;
    }

    public void restart(){
        enfermedades.clear();
        medicamentos.clear();
        pruebas.clear();
        numConsultas = 0;
        maxEnfermedades = 0;
        maxMedicamentos = 0;
        maxPruebas = 0;
        maxSickness = new Pair<>("",0);
        maxMeds = new Pair<>("",0);
        maxLabTests = new Pair<>("",0);
    }
}