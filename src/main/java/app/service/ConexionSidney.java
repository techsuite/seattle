package app.service;

import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConexionSidney {

  public static final MediaType JSON = MediaType.parse(
    "application/json; charset=utf-8"
  );

  @Autowired
  private OkHttpClient client;

  @Autowired
  private LogsService logsService;

  public void lanzarPost() throws Exception {
    JSONObject post = new JSONObject();
    post.put("senderID", "seattle");
    post.put("number", 70);
    System.out.println(post.toString());

    RequestBody body = RequestBody.create(JSON, String.valueOf(post));
    Request request = new Request.Builder()
      .url("https://sidney.juliocastrodev.duckdns.org/getNumb")
      .post(body)
      .build();

    Call call = client.newCall(request);
    Response response = call.execute();

    logsService.basic(""+response.code());
    logsService.basic(response.body().string());
  }
  
}
