package app.service;

import app.factory.SimpleFactory;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class Mail {

  @Autowired
  private JavaMailSender mailSender;

  @Autowired
  private SimpleFactory simpleFactory;

  public void sendMail(String to, String subject, String body)
    throws Exception {
    MimeMessage mimeMessage = mailSender.createMimeMessage();
    MimeMessageHelper helper = simpleFactory.mimeMessageHelper(mimeMessage);

    helper.setTo(to);
    helper.setSubject(subject);
    helper.setText(body, true);

    mailSender.send(mimeMessage);
  }
}
