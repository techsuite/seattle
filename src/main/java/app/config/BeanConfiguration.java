package app.config;

import okhttp3.OkHttpClient;
import org.apache.commons.dbutils.QueryRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

@Configuration
public class BeanConfiguration {

  @Bean
  public QueryRunner queryRunner() {
    return new QueryRunner();
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.basePackage("app.controller"))
      .paths(PathSelectors.any())
      .build();
  }

  @Bean
  public OkHttpClient createOkHttpClient() {
    return new OkHttpClient();
  }

  @Bean
  public AbstractRequestLoggingFilter logFilter() {
    return new CustomLogsFilter();
  }
}
