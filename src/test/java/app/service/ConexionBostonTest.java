package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConexionBostonTest {

  @Autowired
  private ConexionBoston conexionBoston;

  @MockBean
  private OkHttpClient client;

  @MockBean
  private LogsService logsService;

  @Mock
  private Call call;

  @Mock
  private Response response;

  @Mock
  private ResponseBody body;

  @Test
  public void pingBoston_whenCalled_shouldFinishCorrectly() throws Exception {
    when(client.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.code()).thenReturn(200);

    conexionBoston.pingBoston(2);
    verify(logsService, times(0)).pingError(anyInt());
  }

  @Test
  public void pingBoston_whenCalled_shouldntFinishCorrectly() throws Exception {
    when(client.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.code()).thenReturn(404);

    conexionBoston.pingBoston(2);
    verify(logsService, times(2)).pingError(404);
  }

}



