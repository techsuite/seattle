package app.service;

import app.factory.SimpleFactory;
import app.model.SidneyConsulta;
import app.model.SidneyEmpleado;
import app.model.SidneyPaciente;
import app.model.response.SidneyData;
import kotlin.Pair;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CronTest {

    @InjectMocks
    private CronService cronService;

    @Mock
    private Mail mail;

    @Mock
    private MetricasMail metricasMail;

    @Test
    public void metricSendingWorks() throws Exception {

        when(metricasMail.getMaxSickness()).thenReturn(new Pair<>("enfermedad2",2));
        when(metricasMail.getMaxMeds()).thenReturn(new Pair<>("medicamento2",2));
        when(metricasMail.getMaxLabTests()).thenReturn(new Pair<>("pruebas2",2));
        when(metricasMail.getNumConsultas()).thenReturn(4);
        when(metricasMail.numPCR()).thenReturn(3);
        when(metricasMail.numPacientesCovid()).thenReturn(2);

        cronService.metricSending();

        verify(mail).sendMail("developer.techsuite@outlook.com","Metricas ultimas 24h","<div><h1>Techsuite</h1><p>Se han realizado " + 4 + " consultas en estas 24h "  +
                "<br>" +
                "La enfermedad más común estas 24h ha sido "+ "enfermedad2"+" con "+2+" incidencias." +
                "<br>" +
                "El medicamento más recetado estas 24h ha sido " + "medicamento2"+" con "+2+" incidencias."+
                "<br>" +
                "La prueba más realizada estas 24h ha sido "+ "pruebas2"+" con "+2+" incidencias." +
                "<br>" +
                "Se han realizado " + 3 + " pruebas PCR. " +
                "<br>" +
                "Se han detectado " + 2 + " positivos por coronavirus estas últimas 24h. " +
                "</p></div>");

    }

    @Test
    public void cronServiceExceptionCase() throws Exception {
        doThrow(Exception.class).when(mail).sendMail(any(), any(), any());
        cronService.metricSending();
    }


}
