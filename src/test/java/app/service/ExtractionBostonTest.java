package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import app.model.response.BostonData;
import app.model.Metadata;
import com.google.gson.Gson;
import java.util.List;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExtractionBostonTest {

    @Autowired
    private ExtractionBoston extraction;

    @MockBean
    private OkHttpClient client;

    @MockBean
    private ConexionSidney conexionSidney;

    @MockBean
    private LogsService logsService;

    @Mock
    private Call call;

    @Mock
    private Response response;

    @Mock
    private ResponseBody body;
    
    @Test
    public void extractionBoston_whenCalled_shouldFinishCorrectly() throws Exception {

        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(200);
        when(response.body()).thenReturn(body);

        var mockBostonDataBean = new BostonData(List.of(), new Metadata(10, 2, 7));
        when(body.string()).thenReturn(new Gson().toJson(mockBostonDataBean));

        extraction.extraction();
        verify(logsService).firstExtractionStart();
        verify(logsService).firstExtractionEnd();
        verify(logsService).nExtractionStart(2,7);
        verify(logsService).nExtractionStart(3,7);
        verify(logsService).nExtractionStart(4,7);
        verify(logsService).nExtractionStart(5,7);
        verify(logsService).nExtractionStart(6,7);
        verify(logsService).nExtractionStart(7,7);
        verify(logsService, times(0)).extractionError(anyInt());

    }

    @Test
    public void extractionBoston_whenCalled_shouldntFinish() throws Exception {
        
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(404);
        when(response.body()).thenReturn(body);

        extraction.extraction();
        verify(logsService, times(2)).pingError(404);
        verify(logsService).extractionError(404);
        verify(logsService, times(0)).firstExtractionStart();
        verify(logsService, times(0)).firstExtractionEnd();
        verify(logsService, times(0)).nExtractionStart(anyInt(), anyInt());
        verify(logsService, times(0)).nExtractionEnd(anyInt(), anyInt());

    }
}
