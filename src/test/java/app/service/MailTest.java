package app.service;

import static org.mockito.Mockito.*;

import app.factory.SimpleFactory;
import javax.mail.internet.MimeMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTest {

  @InjectMocks
  private Mail mail;

  @Mock
  private JavaMailSender mailSender;

  @Mock
  private SimpleFactory simpleFactory;

  @Mock
  private MimeMessage mimeMessage;

  @Mock
  private MimeMessageHelper mimeMessageHelper;

  @Test
  public void sendMailWorks() throws Exception {
    when(mailSender.createMimeMessage()).thenReturn(mimeMessage);
    when(simpleFactory.mimeMessageHelper(any())).thenReturn(mimeMessageHelper);

    mail.sendMail(
      "developer.techsuite@outlook.com",
      "hola",
      "<div><h1>Techsuite</h1><p>Hola</p></div>"
    );

    verify(mimeMessageHelper).setTo("developer.techsuite@outlook.com");
    verify(mimeMessageHelper).setSubject("hola");
    verify(mimeMessageHelper)
      .setText("<div><h1>Techsuite</h1><p>Hola</p></div>", true);
    verify(mailSender).send(mimeMessage);
  }
}
