package app.service;

import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LogsServiceTest {

    @MockBean
    private LogsService logsService;

    @Test
    public void runAllMethods() throws Exception {
        logsService.extractionError(404);
        logsService.pingError(404);
        logsService.firstExtractionStart();
        logsService.firstExtractionEnd();
        logsService.nExtractionStart(3,3);
        logsService.nExtractionEnd(3,3);
    }
}
