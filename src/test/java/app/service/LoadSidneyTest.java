package app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoadSidneyTest {
    @Autowired
    private LoadSidney loadSidney;

    @MockBean
    private OkHttpClient client;

    @MockBean
    private LogsService logsService;

    @Mock
    private Call call;

    @Mock
    private Response response;

    @Mock
    private ResponseBody body;

    @Test
    public void loadSidney_whenCalled_shouldFinishCorrectly() throws Exception {
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(200);
        when(response.body()).thenReturn(body);

        loadSidney.load("Test", 1, 7);

        verify(call).execute();
        verify(logsService).nLoadStart(1, 7);
        verify(logsService).nLoadEnd(1, 7);
    }

    @Test
    public void loadSidney_whenCalled_shouldntFinishCorrectly() throws Exception {
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(404);
        when(response.body()).thenReturn(body);

        loadSidney.load("Test", 1, 7);

        verify(call).execute();
        verify(logsService).nLoadStart(1, 7);
        verify(logsService).loadError(404);
    }
}
