package app.service;

import static org.assertj.core.api.Assertions.assertThat;

import app.model.SidneyConsulta;
import app.model.SidneyEmpleado;
import app.model.SidneyPaciente;
import app.model.response.SidneyData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MetricasMailTest {

  @Autowired
  private MetricasMail metricasMail;

  @Before
  public void setup() {
    metricasMail.restart();
  }

  @Test
  public void updateWorks() {
    SidneyEmpleado dummyEmpleado = new SidneyEmpleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "fechaNacimientoDoctor1",
      "correoElectronicoDoctor1",
      "generoDoctor1",
      "passwordDoctor1",
      "fake"
    );
    SidneyPaciente dummyPaciente = new SidneyPaciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "fechaNacimientoPaciente1",
      "correoElectronicoPaciente1",
      "generoPaciente1",
      ""
    );
    SidneyConsulta dummyConsulta = new SidneyConsulta(
      1,
      "dniDoctor1",
      "dniPaciente1",
      "fechaRegistroPaciente1",
      "fechaConsulta1",
      new String[] {
        "enfermedad1",
        "enfermedad2",
        "enfermedad3",
        "enfermedad4",
      },
      new String[] { "medicamento1", "medicamento2" },
      new String[] { "pruebas1", "pruebas2", "pruebas3" },
      new String[] { "observacion1", "observacion2" }
    );
    String key = "W6YRf#*ZLsZ1";

    SidneyData dummy = new SidneyData(
      key,
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyConsulta)
    );

    metricasMail.update(dummy);

    Map<String, Integer> enfermedades = new HashMap<>();
    Map<String, Integer> medicamentos = new HashMap<>();
    Map<String, Integer> pruebas = new HashMap<>();

    enfermedades.put("enfermedad1", 1);
    enfermedades.put("enfermedad2", 1);
    enfermedades.put("enfermedad3", 1);
    enfermedades.put("enfermedad4", 1);

    medicamentos.put("medicamento1", 1);
    medicamentos.put("medicamento2", 1);

    pruebas.put("pruebas1", 1);
    pruebas.put("pruebas2", 1);
    pruebas.put("pruebas3", 1);

    assertThat(metricasMail.getEnfermedades()).isEqualTo(enfermedades);
    assertThat(metricasMail.getMedicamentos()).isEqualTo(medicamentos);
    assertThat(metricasMail.getPruebas()).isEqualTo(pruebas);

    Pair<String, Integer> maxSickness;
    maxSickness = new Pair<>("enfermedad1", 1);
    Pair<String, Integer> maxMeds;
    maxMeds = new Pair<>("medicamento1", 1);
    Pair<String, Integer> maxLabTests;
    maxLabTests = new Pair<>("pruebas1", 1);

    assertThat(metricasMail.getMaxSickness()).isEqualTo(maxSickness);
    assertThat(metricasMail.getMaxMeds()).isEqualTo(maxMeds);
    assertThat(metricasMail.getMaxLabTests()).isEqualTo(maxLabTests);

    assertThat(metricasMail.getNumConsultas()).isEqualTo(1);

    metricasMail.restart();
  }

  @Test
  public void updateIfCasesWork() {
    SidneyEmpleado dummyEmpleado = new SidneyEmpleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "fechaNacimientoDoctor1",
      "correoElectronicoDoctor1",
      "generoDoctor1",
      "passwordDoctor1",
      "fake"
    );
    SidneyPaciente dummyPaciente = new SidneyPaciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "fechaNacimientoPaciente1",
      "correoElectronicoPaciente1",
      "generoPaciente1",
      ""
    );
    SidneyConsulta dummyConsulta = new SidneyConsulta(
      1,
      "dniDoctor1",
      "dniPaciente1",
      "fechaRegistroPaciente1",
      "fechaConsulta1",
      new String[] {
        "enfermedad1",
        "enfermedad2",
        "enfermedad2",
        "enfermedad3",
        "enfermedad4",
      },
      new String[] { "medicamento1", "medicamento2", "medicamento2" },
      new String[] { "pruebas1", "pruebas2", "pruebas2", "pruebas3" },
      new String[] { "observacion1", "observacion2" }
    );
    String key = "W6YRf#*ZLsZ1";

    SidneyData dummy = new SidneyData(
      key,
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyConsulta)
    );

    metricasMail.update(dummy);

    Map<String, Integer> enfermedades = new HashMap<>();
    Map<String, Integer> medicamentos = new HashMap<>();
    Map<String, Integer> pruebas = new HashMap<>();

    enfermedades.put("enfermedad1", 1);
    enfermedades.put("enfermedad2", 2);
    enfermedades.put("enfermedad3", 1);
    enfermedades.put("enfermedad4", 1);

    medicamentos.put("medicamento1", 1);
    medicamentos.put("medicamento2", 2);

    pruebas.put("pruebas1", 1);
    pruebas.put("pruebas2", 2);
    pruebas.put("pruebas3", 1);

    assertThat(metricasMail.getEnfermedades()).isEqualTo(enfermedades);
    assertThat(metricasMail.getMedicamentos()).isEqualTo(medicamentos);
    assertThat(metricasMail.getPruebas()).isEqualTo(pruebas);

    Pair<String, Integer> maxSickness;
    maxSickness = new Pair<>("enfermedad2", 2);
    Pair<String, Integer> maxMeds;
    maxMeds = new Pair<>("medicamento2", 2);
    Pair<String, Integer> maxLabTests;
    maxLabTests = new Pair<>("pruebas2", 2);

    assertThat(metricasMail.getMaxSickness()).isEqualTo(maxSickness);
    assertThat(metricasMail.getMaxMeds()).isEqualTo(maxMeds);
    assertThat(metricasMail.getMaxLabTests()).isEqualTo(maxLabTests);

    metricasMail.restart();
  }

  @Test
  public void gettersWork() {
    SidneyEmpleado dummyEmpleado = new SidneyEmpleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "fechaNacimientoDoctor1",
      "correoElectronicoDoctor1",
      "generoDoctor1",
      "passwordDoctor1",
      "fake"
    );
    SidneyPaciente dummyPaciente = new SidneyPaciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "fechaNacimientoPaciente1",
      "correoElectronicoPaciente1",
      "generoPaciente1",
      ""
    );
    SidneyConsulta dummyConsulta = new SidneyConsulta(
      1,
      "dniDoctor1",
      "dniPaciente1",
      "fechaRegistroPaciente1",
      "fechaConsulta1",
      new String[] {
        "enfermedad1",
        "enfermedad2",
        "enfermedad3",
        "enfermedad4",
      },
      new String[] { "medicamento1", "medicamento2" },
      new String[] { "pruebas1", "pruebas2", "pruebas3" },
      new String[] { "observacion1", "observacion2" }
    );
    String key = "W6YRf#*ZLsZ1";

    SidneyData dummy = new SidneyData(
      key,
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyConsulta)
    );

    metricasMail.update(dummy);

    assertThat(metricasMail.getNumSickness()).isEqualTo(4);
    assertThat(metricasMail.getNumMeds()).isEqualTo(2);
    assertThat(metricasMail.getNumLabTests()).isEqualTo(3);

    metricasMail.restart();
  }

  @Test
  public void restartWorks() {
    SidneyEmpleado dummyEmpleado = new SidneyEmpleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "fechaNacimientoDoctor1",
      "correoElectronicoDoctor1",
      "generoDoctor1",
      "passwordDoctor1",
      "fake"
    );
    SidneyPaciente dummyPaciente = new SidneyPaciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "fechaNacimientoPaciente1",
      "correoElectronicoPaciente1",
      "generoPaciente1",
      ""
    );
    SidneyConsulta dummyConsulta = new SidneyConsulta(
      1,
      "dniDoctor1",
      "dniPaciente1",
      "fechaRegistroPaciente1",
      "fechaConsulta1",
      new String[] {
        "enfermedad1",
        "enfermedad2",
        "enfermedad3",
        "enfermedad4",
      },
      new String[] { "medicamento1", "medicamento2" },
      new String[] { "pruebas1", "pruebas2", "pruebas3" },
      new String[] { "observacion1", "observacion2" }
    );
    String key = "W6YRf#*ZLsZ1";

    SidneyData dummy = new SidneyData(
      key,
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyConsulta)
    );

    metricasMail.update(dummy);

    metricasMail.restart();

    assertThat(metricasMail.getEnfermedades()).isEmpty();
    assertThat(metricasMail.getMedicamentos()).isEmpty();
    assertThat(metricasMail.getPruebas()).isEmpty();
    assertThat(metricasMail.getNumConsultas()).isEqualTo(0);
    assertThat(metricasMail.getMaxEnfermedades()).isEqualTo(0);
    assertThat(metricasMail.getMaxMedicamentos()).isEqualTo(0);
    assertThat(metricasMail.getMaxPruebas()).isEqualTo(0);
    assertThat(metricasMail.getMaxSickness()).isEqualTo(new Pair<>("", 0));
    assertThat(metricasMail.getMaxMeds()).isEqualTo(new Pair<>("", 0));
    assertThat(metricasMail.getMaxLabTests()).isEqualTo(new Pair<>("", 0));
    assertThat(metricasMail.getNumConsultas()).isEqualTo(0);
    assertThat(metricasMail.numPCR()).isEqualTo(0);
    assertThat(metricasMail.numPacientesCovid()).isEqualTo(0);
  }
}
