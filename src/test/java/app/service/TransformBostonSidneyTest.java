package app.service;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import app.model.BostonConsulta;
import app.model.Metadata;
import app.model.SidneyConsulta;
import app.model.SidneyEmpleado;
import app.model.SidneyPaciente;
import app.model.response.BostonData;
import app.model.response.SidneyData;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TransformBostonSidneyTest {

    @Autowired
    private TransformBostonSidney transformBSidney;

    @Test
    public void transformBostonSidney_Test() throws Exception {
        
        BostonConsulta dummyBoston = new BostonConsulta(1, "fechaRegistroPaciente1", 
        "fechaConsulta1", "enfermedadesDiagnosticadas1", 
        "medicamentosRecetados1", "pruebasDeLaboratorioARealizar1", "observaciones1"
        , "dniDoctor1", "nombreDoctor1", "apellidosDoctor1", "fechaNacimientoDoctor1", 
        "correoElectronicoDoctor1", "generoDoctor1", "passwordDoctor1", "dniPaciente1"
        , "nombrePaciente1", "apellidosPaciente1", "fechaNacimientoPaciente1", 
        "correoElectronicoPaciente1", "generoPaciente1"); 

        List<BostonConsulta> dummyListBoston = List.of(dummyBoston);

        BostonData mockBostonDataBean = new BostonData(dummyListBoston, new Metadata(1, 1, 1));
        
        SidneyEmpleado dummyEmpleado = new SidneyEmpleado("dniDoctor1", "nombreDoctor1", "apellidosDoctor1", "fechaNacimientoDoctor1", 
        "correoElectronicoDoctor1", "generoDoctor1", "passwordDoctor1","fake");
        SidneyPaciente dummyPaciente = new SidneyPaciente("dniPaciente1"
        , "nombrePaciente1", "apellidosPaciente1", "fechaNacimientoPaciente1", 
        "correoElectronicoPaciente1", "generoPaciente1","");
        SidneyConsulta dummyConsulta = new SidneyConsulta(1, "dniDoctor1", "dniPaciente1",
         "fechaRegistroPaciente1","fechaConsulta1", new String[]{"enfermedadesDiagnosticadas1"}, 
        new String[]{"medicamentosRecetados1"}, new String[]{"pruebasDeLaboratorioARealizar1"}, 
        new String[]{"observaciones1"});
        String key = "W6YRf#*ZLsZ1";

        SidneyData dummy = new SidneyData(key,List.of(dummyEmpleado), 
        List.of(dummyPaciente), List.of(dummyConsulta));
        
        SidneyData output = transformBSidney.transform(mockBostonDataBean);

        assertThat(output).isEqualTo(dummy);

    }

    @Test
    public void transformBostonSidney_TestWithSplit() throws Exception {
        
        BostonConsulta dummyBoston = new BostonConsulta(1, "fechaRegistroPaciente1", 
        "fechaConsulta1", "enfermedad1.enfermedad2.enfermedad3.enfermedad4", 
        "medicamento1.medicamento2", "pruebas1.pruebas2.pruebas3", 
        "observacion1.observacion2" , "dniDoctor1", "nombreDoctor1", 
        "apellidosDoctor1", "fechaNacimientoDoctor1", "correoElectronicoDoctor1", "generoDoctor1", "passwordDoctor1", "dniPaciente1"
        , "nombrePaciente1", "apellidosPaciente1", "fechaNacimientoPaciente1", 
        "correoElectronicoPaciente1", "generoPaciente1"); 

        List<BostonConsulta> dummyListBoston = List.of(dummyBoston);

        BostonData mockBostonDataBean = new BostonData(dummyListBoston, new Metadata(1, 1, 1));
        
        SidneyEmpleado dummyEmpleado = new SidneyEmpleado("dniDoctor1", "nombreDoctor1", "apellidosDoctor1", "fechaNacimientoDoctor1", 
        "correoElectronicoDoctor1", "generoDoctor1", "passwordDoctor1","fake");
        SidneyPaciente dummyPaciente = new SidneyPaciente("dniPaciente1"
        , "nombrePaciente1", "apellidosPaciente1", "fechaNacimientoPaciente1", 
        "correoElectronicoPaciente1", "generoPaciente1","");
        SidneyConsulta dummyConsulta = new SidneyConsulta(1, "dniDoctor1", "dniPaciente1", "fechaRegistroPaciente1",
        "fechaConsulta1", new String[]{"enfermedad1","enfermedad2","enfermedad3","enfermedad4"}, 
        new String[]{"medicamento1","medicamento2"}, new String[]{"pruebas1","pruebas2","pruebas3"}, 
        new String[]{"observacion1","observacion2"} );
        String key = "W6YRf#*ZLsZ1";

        SidneyData dummy = new SidneyData(key,List.of(dummyEmpleado), 
        List.of(dummyPaciente), List.of(dummyConsulta));
        
        SidneyData output = transformBSidney.transform(mockBostonDataBean);

        assertThat(output).isEqualTo(dummy);

    }

    @Test
    public void transformBostonSidney_TestWithVacios() throws Exception {
        BostonConsulta dummyBoston = new BostonConsulta(1, "fechaRegistroPaciente1", 
        "fechaConsulta1", " ", "medicamento1.medicamento2", " ", 
        "observacion1.observacion2" , "dniDoctor1", "nombreDoctor1", 
        " ", "fechaNacimientoDoctor1", "correoElectronicoDoctor1", 
        "generoDoctor1", "passwordDoctor1", "dniPaciente1", "nombrePaciente1",
        " ", "fechaNacimientoPaciente1", "correoElectronicoPaciente1", " "); 

        List<BostonConsulta> dummyListBoston = List.of(dummyBoston);

        BostonData mockBostonDataBean = new BostonData(dummyListBoston, new Metadata(1, 1, 1));
        
        SidneyEmpleado dummyEmpleado = new SidneyEmpleado("dniDoctor1", "nombreDoctor1", 
        "", "fechaNacimientoDoctor1", "correoElectronicoDoctor1", 
        "generoDoctor1", "passwordDoctor1","fake");
        SidneyPaciente dummyPaciente = new SidneyPaciente("dniPaciente1"
        , "nombrePaciente1", "" , "fechaNacimientoPaciente1", 
        "correoElectronicoPaciente1", "" ,"");
        SidneyConsulta dummyConsulta = new SidneyConsulta(1, "dniDoctor1", "dniPaciente1", "fechaRegistroPaciente1",
        "fechaConsulta1", new String[]{}, new String[]{"medicamento1","medicamento2"}, new String[]{},
        new String[]{"observacion1","observacion2"});
        String key = "W6YRf#*ZLsZ1";

        SidneyData dummy = new SidneyData(key,List.of(dummyEmpleado), 
        List.of(dummyPaciente), List.of(dummyConsulta));
        
        SidneyData output = transformBSidney.transform(mockBostonDataBean);

        assertThat(output).isEqualTo(dummy);

    }
    
}
